import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue'
import commonjs from 'vite-plugin-commonjs';
import dts from 'vite-plugin-dts';
import * as path from 'path';

export default defineConfig({
  // plugins: [commonjs(), vue(), cdsPlugin, dts()],
  plugins: [commonjs(), vue(), dts()],
  resolve: {
    // alias: [{ find: "@", replacement: path.resolve(__dirname, "./src") }]
    // alias: {
    //   "@/": new URL("./src/", import.meta.url).pathname,
    // },
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
  build: {
    target: "esnext",
    lib: {
      entry: './src/index.ts',
      name: 'EdsJsSignWidget',
      formats: [ 'es', 'cjs', 'umd', 'iife' ],
    },
    rollupOptions: {
      external: ['vue', '@central-design-system/components', '@b2b-frontend/widget-client'],
      output: {
        dir: './dist',
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
})
