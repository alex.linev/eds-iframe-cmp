import {ref} from 'vue';
import type {TSignature, TSignContext} from '@/types';
import {ESignSoftware, ESignStrategy} from '@/types';
import type {ISignServiceAsync} from '@/provider/BrowserPlugin/SignService';

export function useCreateSignature(signService: ISignServiceAsync) {
    const isSigning = ref(false);
    const signature = ref<TSignature | null>(null);
    const createSignatureError = ref<Error | null>(null);

    const createSignature = async (ctx: TSignContext): Promise<void> => {
        isSigning.value = true;
        const {content, strategy} = ctx;
        if (content.type !== 'text') {
            createSignatureError.value = new Error('Неподходящий тип контекста');
            isSigning.value = false;
            return;
        }
        const {type, software} = strategy
        if (content.type === 'text' && type === ESignStrategy.Qualified && software === ESignSoftware.CPro) {
            try {
                signature.value = await signService.signText(content.unsigned_message, strategy.certificate_identifier);
            } catch (e) {
                console.error(e);
                createSignatureError.value = new Error('Не удалось сформировать подпись');
            }
        } else {
            createSignatureError.value = new Error('Неподходящий контекст');
        }
        isSigning.value = false;
    }

    return {
        isSigning,
        signature,
        createSignature,
        createSignatureError,
    }
}