import {ref} from 'vue';
import type {IBackendApiServiceProvider, TSignConfiguration, TSignContext} from '@/types';

export default function useSignConfigurationFetch(configProvider: IBackendApiServiceProvider) {
    const isSignConfigurationFetching = ref(false);
    const signConfiguration = ref<TSignConfiguration | null>(null);
    const signConfigurationFetchError = ref<Error | null>(null);

    const fetchSignConfiguration = async (ctx: TSignContext): Promise<void> => {
        console.log('loading signConfiguration');
        isSignConfigurationFetching.value = true;
        try {
            const config = await configProvider.provideSignConfiguration(ctx);
            isSignConfigurationFetching.value = false;
            signConfiguration.value = config;
        } catch (e) {
            signConfigurationFetchError.value = new Error('configuration errror');
            isSignConfigurationFetching.value = false;
        }
    }

    return {
        signConfiguration,
        fetchSignConfiguration,
        isSignConfigurationFetching,
        signConfigurationFetchError
    };
}