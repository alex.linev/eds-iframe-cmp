import {ref} from 'vue';
import type {IBrowserPluginStoreAsync, TCertificatesMap} from '@/provider/BrowserPlugin/StoreService';

export default function useCertificateListFetch(store: IBrowserPluginStoreAsync) {
    const isCertificatesFetching = ref<boolean>(false);
    const certificates = ref<TCertificatesMap | null>(null);
    const certificatesFetchError = ref<Error | null>(null);
    const fetchCertificateList = async (): Promise<void> => {
        certificates.value = null;
        isCertificatesFetching.value = true;
        certificatesFetchError.value = null;
        try {
            certificates.value = await store.getCertificates();
        } catch (e) {
            certificatesFetchError.value = new Error('Не удалось извлечь сертификаты из хранилища');
        } finally {
            isCertificatesFetching.value = false;
        }
    }
    return {
        isCertificatesFetching,
        certificates,
        fetchCertificateList,
        certificatesFetchError
    }
}