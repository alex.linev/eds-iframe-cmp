import type {IEnvironment} from '@/provider/BrowserPlugin';
import {ref} from 'vue';
import {Environment} from '@/provider/BrowserPlugin';
import CadesBrowserPluginAsync from '@/provider/BrowserPlugin/CadesBrowserPluginAsync';

export function useBrowserPluginConnector() {
    const env = ref<IEnvironment | null>(null);
    const browserPluginFactory = ref<CadesBrowserPluginAsync | null>(null);
    const connectBrowserPluginFactoryError = ref<Error | null>(null);
    const isConnecting = ref(false);

    const getBrowserPluginFactory = async (): Promise<void> => {
        env.value = Environment.i();
        isConnecting.value = true;
        try {
            browserPluginFactory.value = await CadesBrowserPluginAsync.initByEnvironment(env.value);
        } catch (e) {
            console.error(e);
            connectBrowserPluginFactoryError.value = new Error('Plugin connection error');
        } finally {
            isConnecting.value = false;
        }
    }
    return {env, getBrowserPluginFactory, browserPluginFactory, connectBrowserPluginFactoryError};
}