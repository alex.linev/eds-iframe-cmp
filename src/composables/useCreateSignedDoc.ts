import {ref} from 'vue';
import type {IBackendApiServiceProvider, TSignConfiguration, TSignContext, TSignedDocument} from '@/types';

export default function useCreateSignedDoc(configProvider: IBackendApiServiceProvider) {
    const isSignedDocCreating = ref(false);
    const signedDoc = ref<TSignedDocument | null>(null);
    const createSignedDocError = ref<Error | null>(null);

    const createSignedDoc = async (ctx: TSignContext): Promise<void> => {
        isSignedDocCreating.value = true;
        try {
            const doc = await configProvider.createSignedDocument(ctx);
            isSignedDocCreating.value = false;
            signedDoc.value = ctx.doc as TSignedDocument;
        } catch (e: unknown) {
            // if (e instanceof Response && 'error' in e && 'title' in e) { //fixme
            //     // const {title, ok} = e.error;
            //     createSignedDocError.value = new Error(((e.error as ).title) || 'create doc error');
            //     isSignedDocCreating.value = false;
            // } else {
                createSignedDocError.value = new Error('create doc error');
                isSignedDocCreating.value = false;
            // }
        }
    }

    return {
        signedDoc,
        createSignedDoc,
        isSignedDocCreating,
        createSignedDocError
    };
}