import type {ISignConfigureStrategy, TConfigureRequest, TCreateSignConfiguration} from '@/types';

export default abstract class SignConfigureStrategy implements ISignConfigureStrategy {
    abstract configure(cfg: TConfigureRequest): Promise<TCreateSignConfiguration>;
}