import SignConfigureStrategy from '@/lib/CreateSignedDocument/Strategy/SignConfigureStrategy';
import type {
    TConfigureRequest,
    TCreateSignConfiguration,
    TSignSoftwareOptions
} from '@/types';

export default class InplaceSignConfigureStrategy extends SignConfigureStrategy{
    constructor(
        private readonly softCfg: TSignSoftwareOptions
    ) {
        super();
    }

    configure(cfg: TConfigureRequest): Promise<TCreateSignConfiguration> {
        const result: TCreateSignConfiguration = {
            type: 'text',
            softwareOptions: this.softCfg
        }
        return Promise.resolve(result);
    }

}