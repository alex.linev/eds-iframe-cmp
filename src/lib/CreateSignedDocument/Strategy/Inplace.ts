import type { TSignConfiguration, TCreateSignedDocumentStrategy, TSignContext } from '../../../types';

/**
 * @author API Team <Api-dev-team@b2b-center.ru>
 */
export default class Inplace implements TCreateSignedDocumentStrategy {
  constructor(readonly config: any, readonly signHandler: (signature: string) => void) {}

  async getSignConfiguration(): Promise<TSignConfiguration> {
    return Promise.resolve(this.config);
  }

  async onSigned(context: TSignContext, signature: string): Promise<void> {
    this.signHandler(signature);
  }
}
