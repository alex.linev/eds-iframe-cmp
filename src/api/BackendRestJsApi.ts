/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

/** Описывает ошибку */
export interface Error {
  title: string;
  description?: string;
  api_code: string;
  trace_id?: TraceID;
}

export type FileId = number;

export interface UserInfo {
  type?: 'bearer_in_header';
}

export type SignContentType = 'text' | 'files';

export type TraceID = string;

export type SignedDocumentId = number;

export type Signature = string;

export type SignatureType = 'BES' | 'XLT1';

export type Hash = string;

export type Hashes = Hash[];

export interface FileHash {
  file_id: FileId;
  hash: Hash;
}

export type FileHashList = FileHash[];

/** Данные по зарегистрированным за пользователем сертификатам. На КЗ может быть оба вида сертификата у пользователя одновременно */
export interface QualifiedCertificateConfigItem {
  /** Для плагина КриптоПро используется отпечаток при поиске сертификатов в хранилище, у NCA - БИН */
  identifier_type: 'thumbprint' | 'bin';
  identifier: string;
  software: 'CPro' | 'NCA';
}

export interface QualifiedSignConfig {
  valid: QualifiedCertificateConfigItem[];
}

export interface SimpleSignConfig {
  owner: string;
  org_name: string;
}

export interface MockQualifiedConfig {
  user_name?: string;
  owner: string;
  org_name?: string;
  issuer: string;
  serial_number: string;
  thumbprint: string;
  valid_to: string;
  valid_from: string;
  algorithm?: string;
  certificate_body?: string;
}

/** Фейковые данные для заглушки (подпись, сертификат), которые необходимы, чтобы имитировать процесс формирования ЭП */
export interface MockSignConfig {
  signature?: Signature;
  /** Фейковые данные для отображения их как данные сертификата текущего пользователя */
  qualified?: MockQualifiedConfig[];
}

export interface SignConfigRequest {
  sign_content_type?: SignContentType;
  user_info?: UserInfo;
}

export type SignFilesConfigRequest = SignConfigRequest & {
  file_ids?: FileId[];
};

export type SignConfigResponse = BaseSignConfigResponse &
  (BaseSignConfigResponseTypeMapping<'text', any> | BaseSignConfigResponseTypeMapping<'files', any>);

export type SignFilesConfigResponse = SignConfigResponse & {
  type?: 'files';
  hashes?: {
    file_id: number;
    hash: Hash;
  }[];
};

export interface SignedTextDocumentCreateRequest {
  content: string;
  sign_content_type: SignContentType;
  signature: Signature;
  user_info: UserInfo;
}

export interface SignedDocumentCreatedResponse {
  doc_id?: SignedDocumentId;
}

interface BaseSignConfigResponse {
  type: 'text';
  qualified?: QualifiedSignConfig;
  simple?: SimpleSignConfig;
  /** Фейковые данные для заглушки (подпись, сертификат), которые необходимы, чтобы имитировать процесс формирования ЭП */
  mock?: MockSignConfig;
}

type BaseSignConfigResponseTypeMapping<Key, Type> = {
  type: Key;
} & Type;

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, 'body' | 'bodyUsed'>;

export interface FullRequestParams extends Omit<RequestInit, 'body'> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, 'body' | 'method' | 'query' | 'path'>;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, 'baseUrl' | 'cancelToken' | 'signal'>;
  securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = 'application/json',
  FormData = 'multipart/form-data',
  UrlEncoded = 'application/x-www-form-urlencoded',
  Text = 'text/plain'
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = 'http://b2b-center.ru.test/eds/external_operation/v1';
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) => fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: 'same-origin',
    headers: {},
    redirect: 'follow',
    referrerPolicy: 'no-referrer'
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  protected encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(typeof value === 'number' ? value : `${value}`)}`;
  }

  protected addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  protected addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join('&');
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter(key => 'undefined' !== typeof query[key]);
    return keys
      .map(key => (Array.isArray(query[key]) ? this.addArrayQueryParam(query, key) : this.addQueryParam(query, key)))
      .join('&');
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : '';
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === 'object' || typeof input === 'string') ? JSON.stringify(input) : input,
    [ContentType.Text]: (input: any) => (input !== null && typeof input !== 'string' ? JSON.stringify(input) : input),
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === 'object' && property !== null
            ? JSON.stringify(property)
            : `${property}`
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input)
  };

  protected mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {})
      }
    };
  }

  protected createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === 'boolean' ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(`${baseUrl || this.baseUrl || ''}${path}${queryString ? `?${queryString}` : ''}`, {
      ...requestParams,
      headers: {
        ...(requestParams.headers || {}),
        ...(type && type !== ContentType.FormData ? { 'Content-Type': type } : {})
      },
      signal: (cancelToken ? this.createAbortSignal(cancelToken) : requestParams.signal) || null,
      body: typeof body === 'undefined' || body === null ? null : payloadFormatter(body)
    }).then(async response => {
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then(data => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch(e => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }
      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title Схема API для хранилища подписанных документов
 * @version 1.0.1
 * @baseUrl http://b2b-center.ru.test/eds/external_operation/v1
 *
 * Описывает конечные точки для проверки, сохранения подписанного документа ЭП В хранилище.  Так же описаны вспомогательные для этого процесса действия (конфиг для виджета, в зависимости от возможностей пользователя и окружения, формирование архива и визуализации документа).
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  api = {
    /**
     * No description
     *
     * @name EdsSignConfigTextCreate
     * @request POST:/Api/eds/sign_config_text
     * @response `200` `SignConfigResponse` Конфиг для Js с необходимыми данными для формаирования ЭП текста
     * @response `400` `Error`
     */
    edsSignConfigTextCreate: (data: SignConfigRequest, params: RequestParams = {}) =>
      this.request<SignConfigResponse, Error>({
        path: `/api/eds/sign_config_text`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @name EdsSignConfigFilesCreate
     * @request POST:/Api/eds/sign_config_files
     * @response `200` `SignFilesConfigResponse` Конфиг для Js с необходимыми данными для формаирования ЭП списка файлов из FileStorage
     * @response `400` `Error`
     */
    edsSignConfigFilesCreate: (data: SignFilesConfigRequest, params: RequestParams = {}) =>
      this.request<SignFilesConfigResponse, Error>({
        path: `/api/eds/sign_config_files`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @name EdsSignedDocumentTextCreate
     * @request POST:/Api/eds/signed_document_text
     * @response `200` `SignedDocumentCreatedResponse` Created
     * @response `400` `Error`
     */
    edsSignedDocumentTextCreate: (data: SignedTextDocumentCreateRequest, params: RequestParams = {}) =>
      this.request<SignedDocumentCreatedResponse, Error>({
        path: `/api/eds/signed_document_text`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params
      })
  };
}
