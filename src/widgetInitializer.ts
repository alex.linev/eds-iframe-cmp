import { createApp } from 'vue'
import App from '@/app/App.vue'
import { createCds, components, directives } from '@central-design-system/components';
import {icons} from '@central-design-system/icons';
import '@central-design-system/components/dist/cds.css';

type TWidgetConfig = {
    'backendApiUrl': string;
}

export type TWidgetInitConfig = {
    elementId: string;
    options: TWidgetConfig
}

export function initWidget(cfg: TWidgetInitConfig) {
    const cds = createCds({
        components,
        directives,
        icons
        // ...options
    });
    const app = createApp(App, cfg.options);
    app.use(cds)
    app.mount(`#${cfg.elementId}`);
}