import {IBackendApiServiceProvider, TError, TSignConfiguration, TSignContext, TSignedDocument} from '@/types';
import {
    Api as RestJsOpenApiModel,
    SignConfigRequest,
    SignedDocumentCreatedResponse,
    SignedTextDocumentCreateRequest
} from '@/api/BackendRestJsApi';
import * as console from 'node:console';
import {types} from 'sass';
import Error = types.Error;

export default class BackendRestJsApiProvider implements IBackendApiServiceProvider {

    private readonly restOpenApiModel: RestJsOpenApiModel<any>;

    constructor(
        accessToken?: string
    ) {
        const token =
            accessToken ??
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiXSwianRpIjoiYjJjM2RiMmEtZTlkYS00Yzg0LWIzNGEtODc5YzZjYWJkMWY2IiwiYXVkIjoiZGI3MGMwNDgtNzQzNS00OTE3LWI0ZDEtMTFhNzg0NTE0ZGQ3IiwibXN1YiI6bnVsbCwic2lkIjoiMTlhMzg4NTItOGQ4Mi00ZTJiLThjODItMDA2N2E1YzE3ZDQ0Iiwic3ViIjoiNjAxMzYiLCJpYXQiOjE3MDk1NjM0NjUsImV4cCI6MTcwOTU5OTQ2NX0.AdfdTCEWVWXyTzu680uPZIXVLTkyOOLWINeasWc_Z8iWA81x-VOg_YAK4uB5h5711DkyP3EIG1Tn8DilVKku1zNqAQuH5lBt3Qh26ExB-LXBFg1yzMTHaja19-DIFXRu5A_cyT1Jy1aCdC9Po_BNefexPSpFSrruwAND4MNwjgq74-jR';
        const resultConfig = {
            baseUrl: 'https://iframe-mvp.b2b.test'
        } as { baseUrl: string; baseApiParams: any };
        const headers = { Authorization: `Bearer ${token}` };
        resultConfig.baseApiParams = { headers };
        this.restOpenApiModel = new RestJsOpenApiModel(resultConfig);
    }

    async provideSignConfiguration(ctx: TSignContext): Promise<TSignConfiguration> {
        const req: SignConfigRequest = {
            sign_content_type: 'text',
            user_info: {
                type: 'bearer_in_header'
            }
        };
        const { data, ok, error } = await this.restOpenApiModel.api.edsSignConfigTextCreate(req);
        if (!ok) {
            throw error ?? new Error('При запросе конфига упало с ошибкой');
        }
        const { payload } = data; // FIXME: решить, надо ли тут так или нефиг?
        const { qualified } = payload;
        let qual = undefined;
        if (qualified) {
            const { identifier_type, valid_certificate_identifiers } = qualified;
            qual = { identifier_type, valid_certificate_identifiers };
        }
        const cfg: TSignConfiguration = {
            context: ctx
        };
        if (qual) {
            cfg.qualified = qual;
        }
        return cfg;
    }

    async createSignedDocument(context: TSignContext): Promise<void> {
        if (context.content.type !== 'text') {
            throw new Error('No text');
        }
        const req: SignedTextDocumentCreateRequest = {
            sign_content_type: 'text',
            content: (context.content.unsigned_message) as string,
            signature: context.signature as string,
            user_info: {
                type: 'bearer_in_header'
            }
        };
        const {
            data,
            ok,
            error
        } = await this.restOpenApiModel.api.edsSignedDocumentTextCreate(req);
        if (!ok) {
            const err = new Error(error.title || 'Ошибка при попытке сохранить подписанный текстовый документ в хран');
            context.error = err as TError;
            throw err;
        }
        if (!data.doc_id) {
            // FIXME
            throw new Error('ata.doc_id');
        }
        // FIXME: Херня какая то
        const doc: TSignedDocument = {
            doc_id: data.doc_id
        };
        context.doc = doc;
    }
}