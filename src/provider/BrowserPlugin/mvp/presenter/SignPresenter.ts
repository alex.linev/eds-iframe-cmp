/**
 * @author API Team <api-dev-team@b2b-center.ru>
 */

import type SignService from '../../SignService';
import type { TCertificatesMap, TThumbprint } from '../../StoreService';
import type BrowserPluginStoreAsync from '../../StoreService';
import type { TSignature } from '../model/types';

export interface ISignWithCertificateView {
  onInited(): void;
  onError(error: Error): void;
  onCertificatesLoaded(certificates: TCertificatesMap): void;
  onSigned(signature: TSignature): void;
}

export interface ISignWithCertificatePresenter {
  onInit(): void;
  onSign(text: string, certificate: TThumbprint): Promise<void>;
}

export default class SignWithCertificateSelectPresenter implements ISignWithCertificatePresenter {
  private readonly _signService: SignService;
  private readonly _storeService: BrowserPluginStoreAsync;
  private readonly _view: ISignWithCertificateView;

  constructor(view: ISignWithCertificateView, signSrv: SignService, storeSrv: BrowserPluginStoreAsync) {
    this._view = view;
    this._signService = signSrv;
    this._storeService = storeSrv;
  }

  async onSign(text: string, certificate: string): Promise<void> {
    console.log('pres onSign', text, certificate);
    const signature: TSignature = 'BASE64SIGNATUE';
    return new Promise(resolve => {
      setTimeout(() => {
        this._view.onSigned(signature);
        resolve();
      }, 100);
    });
  }

  async onInit(): Promise<void> {
    console.log('Presenter initialization', this._signService, this._storeService);
    return Promise.resolve();
  }

  async onCertificatesLoad(): Promise<void> {
    const certs = new Map();
    return new Promise(resolve => {
      setTimeout(() => {
        this._view.onCertificatesLoaded(certs);
        resolve();
      }, 100);
    });
  }
}
