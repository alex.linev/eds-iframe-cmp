/**
 * @author API Team <api-dev-team@b2b-center.ru>
 */

import { type TSignature } from './types';

export enum EOperationStatusValue {
  IDLE,
  PENDING,
  DONE
}

export enum EOperationResultType {
  Failed,
  Ok
}

export type TOperationResultError = Error;

export type TOperationResult<R> =
  | {
      type: EOperationResultType.Failed;
      error: TOperationResultError;
    }
  | {
      type: EOperationResultType.Ok;
      value: R;
    };

export type TOperationState<R> =
  | {
      status: EOperationStatusValue.IDLE;
    }
  | {
      status: EOperationStatusValue.PENDING;
      description: string;
    }
  | {
      status: EOperationStatusValue.DONE;
      result: TOperationResult<R>;
    };

export function newOperationState<R>(): TOperationState<R> {
  return { status: EOperationStatusValue.IDLE };
}

export function newSignOperationState(ctx: TSignContext): TSignOperationState {
  return { ...newOperationState(), context: ctx };
}

export type TSignOperationResult = {
  signature: TSignature;
};

export type TSignOperationState = TOperationState<TSignOperationResult> & {
  context: TSignContext;
};

export type TSignWithCertificateSelectionOperationState = TSignOperationState & {
  certificates: [];
  selectedCertificateIdentifier: string;
};

export type TSignSimpleState = TSignOperationState & {
  pin: string;
};

export type TLoadCertificatesResult<CT> = {
  certificates: CT[];
};

export enum ESignContentType {
  TEXT,
  HASH
}

export enum EContentEncodingType {
  NONE,
  BASE64
}

export enum ESignSoftware {
  CPRO,
  NCA,
  SIMPLE
}

export type TSignTextContext = {
  type: ESignContentType.TEXT;
  text: string;
  encoding: EContentEncodingType;
  software: ESignSoftware;
};

export type TSignHashContext = {
  type: ESignContentType.HASH;
  hash: string;
  software: ESignSoftware;
};

// TODO: Надо что то думать с тем, что способы могут быть разные сразу. Вероятно, это вообще отдельная внешняя тема!
export type TSignContext = TSignTextContext | TSignHashContext;
