import {
  EOperationResultType,
  EOperationStatusValue,
  newSignOperationState,
  type TSignContext,
  type TSignOperationState
} from './SignState';

export default class SignModel {
  private _state: TSignOperationState;

  private constructor(state: TSignOperationState) {
    this._state = state;
  }

  static new(ctx: TSignContext): SignModel {
    return new SignModel(newSignOperationState(ctx));
  }

  get state(): TSignOperationState {
    return this._state;
  }

  // FIXME: Может это все неправильно и модель должна возвращать данные, а вот презентер пусть меняет свой стейт?
  async signText(text: string, certificateIdentifier: string): Promise<void> {
    console.log('Model create signature invoked with ', text, certificateIdentifier);
    return new Promise((resolve): void => {
      setTimeout(() => {
        this._state = {
          ...this._state,
          status: EOperationStatusValue.DONE,
          result: { type: EOperationResultType.Failed, error: new Error('nonono') }
        };
        resolve();
      }, 10000);
    });
  }
}
