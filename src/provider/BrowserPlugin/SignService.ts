import type CadesBrowserPluginAsync from './CadesBrowserPluginAsync';
import type {IBrowserPluginStoreAsync, TThumbprint} from './StoreService';
import { CADESCOM_ATTRIBUTE, CADESCOM_CADES_TYPE } from './cadescom';
import { CAPICOM_CERTIFICATE_INCLUDE_OPTION, type IStoreAsync } from './capicom';
import type {TSignature} from '@/types';

export interface ISignServiceAsync {
  signText(text: string, certificateIdentifier: TThumbprint): Promise<TSignature>;
}

export default class SignService implements ISignServiceAsync {
  constructor(private readonly _plugin: CadesBrowserPluginAsync, private readonly _store: IBrowserPluginStoreAsync) { }

  public async signText(text: string, certificateIdentifier: TThumbprint): Promise<TSignature> {
    const cert = await this._store.getCertificate(certificateIdentifier);
    // console.log('REQUESTED SIGN WITH CERT ', certificateIdentifier, await cert.propset_Certificate);
    const signer = await this._plugin.i.CreateObjectAsync('CAdESCOM.CPSigner');
    await signer.propset_Certificate(cert);
    await signer.propset_Options(CAPICOM_CERTIFICATE_INCLUDE_OPTION.CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN);
    const signedData = await this._plugin.i.CreateObjectAsync('CAdESCOM.CadesSignedData');
    // TODO: cosign
    const timeAttr = await this._plugin.i.CreateObjectAsync('CAdESCOM.CPAttribute');
    await timeAttr.propset_Name(CADESCOM_ATTRIBUTE.CADESCOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME);
    await timeAttr.propset_Value(new Date());

    // FIXME: тут по какой то причине нет такого св-ва
    // const authAttr2 = await signer.AuthenticatesAttributes2;
    await signedData.propset_Content(text);
    const signature = await signedData.SignCades(signer, CADESCOM_CADES_TYPE.CADESCOM_CADES_BES, false);
    const result = signature.replace(/[^\x00-\x7F]/g, "").replace(/(\r\n|\n|\r)/gm,"");
    console.info('created signature is ', result);
    return result;
  }
}
