/**
 * @author API Team <api-dev-team@b2b-center.ru>
 */
import { Environment } from './Environment';
import CadesBrowserPluginAsync from './CadesBrowserPluginAsync';

enum LogLevel {
  LOG_LEVEL_DEBUG = 4,
  LOG_LEVEL_INFO = 2,
  LOG_LEVEL_ERROR = 1
}

export class CadesBrowserPluginLoader implements Promise<CadesBrowserPluginAsync> {
  private _pluginPromise?: Promise<CadesBrowserPluginAsync>;

  //note: CryptoPro ugly plugin code
  readonly LOG_LEVEL_DEBUG: LogLevel = LogLevel.LOG_LEVEL_DEBUG;
  readonly LOG_LEVEL_INFO: LogLevel = LogLevel.LOG_LEVEL_INFO;
  readonly LOG_LEVEL_ERROR: LogLevel = LogLevel.LOG_LEVEL_ERROR;

  current_log_level: number = LogLevel.LOG_LEVEL_ERROR; //tslint:disable-line variable-name

  private get _plugin(): Promise<CadesBrowserPluginAsync> {
    if (!this._pluginPromise) {
      this._pluginPromise = CadesBrowserPluginAsync.initByEnvironment(Environment.i());
    }
    return this._pluginPromise;
  }

  get [Symbol.toStringTag](): 'Promise' {
    return 'Promise';
  }

  then<TF = CadesBrowserPluginAsync, TC = never>(
    onFulfilled?: ((value: CadesBrowserPluginAsync) => PromiseLike<TF> | TF) | null,
    onRejected?: ((reason: any) => PromiseLike<TC> | TC) | null
  ): Promise<TF | TC> {
    return this._plugin.then(onFulfilled, onRejected);
  }

  catch<TC = never>(
    onRejected?: ((reason: any) => PromiseLike<TC> | TC) | null
  ): Promise<CadesBrowserPluginAsync | TC> {
    return this._plugin.catch(onRejected);
  }

  finally(onFinally?: (() => void) | undefined | null): Promise<CadesBrowserPluginAsync> {
    return this._plugin.finally(onFinally);
  }
}
