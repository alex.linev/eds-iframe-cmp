import {Environment, EPluginConnectionType, IEnvironment} from './Environment';
import {
    CadesPluginExtensionGlobalObject,
    type ICadesPluginAsync,
    connectExtensionWythStaticUrl,
    connectWithDynamicUrl
} from './CadesExtensionExposeObject';

/**
 * @author API Team <api-dev-team@b2b-center.ru>
 */
export default class CadesBrowserPluginAsync {
    private constructor(readonly _providerInstance: ICadesPluginAsync) {
    }

    static async initByEnvironment(environment: IEnvironment): Promise<CadesBrowserPluginAsync> {
        console.log('try create bp by environment', environment);
        if (!environment.canPromise) {
            throw new Error('По какой то причине нет Promise, для расширения нужен полифил или экспотнуть его в window');
        }
        const {pluginConnectionInfo} = environment;
        console.log('Plugin connection info is', pluginConnectionInfo);
        switch (pluginConnectionInfo.type) {
            case EPluginConnectionType.ExtensionWithStaticUrl:
                return await CadesBrowserPluginAsync.createWithStaticUrl(pluginConnectionInfo.connectionUrl);
            case EPluginConnectionType.ExtensionWithDynamicUrl:
                return await CadesBrowserPluginAsync.createWithUrlResolve();
            default:
                throw new Error('Wrong environment for async plugin wrapper passed');
        }
    }

    static async createWithStaticUrl(url: string): Promise<CadesBrowserPluginAsync> {
        const cadesplugin = await new CadesPluginExtensionGlobalObject(connectExtensionWythStaticUrl(url));
        return new CadesBrowserPluginAsync(cadesplugin);
    }

    // FIXME: Удалить одинаковые методы
    static async createWithUrlResolve(): Promise<CadesBrowserPluginAsync> {
        const cadesplugin = await new CadesPluginExtensionGlobalObject(connectWithDynamicUrl());
        return new CadesBrowserPluginAsync(cadesplugin);
    }

    public get i(): ICadesPluginAsync {
        return this._providerInstance;
    }
}
