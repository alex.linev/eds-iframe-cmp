import { type IObjectNamesMapAsync, LogLevel } from './types';

export interface ICadesPluginAsync {
  // TODO: Подумать над неймингом этого
  CreateObjectAsync<T extends keyof IObjectNamesMapAsync>(objName: T): Promise<IObjectNamesMapAsync[T]>; // TODO: Прописать возможные варианты в виде keyof IObjNamesAsync
  ReleasePluginObjects(): Promise<boolean>;
}

type TInitFn = Promise<ICadesPluginAsync>;

interface ILoadedScript {
  remove(): void;
}

async function loadScript(
  url: string,
  params?: string | [string, string][] | URLSearchParams | null,
  scriptElementOptions: Partial<HTMLScriptElement> = { async: true, defer: true }
): Promise<ILoadedScript> {
  return new Promise<ILoadedScript>((resolve, reject) => {
    const scriptEl: HTMLScriptElement = document.createElement('script');
    scriptEl.type = 'text/javascript';
    Object.assign(scriptEl, scriptElementOptions);
    scriptEl.addEventListener('load', () => {
      resolve({
        remove(): void {
          document.removeChild(scriptEl);
        }
      });
    });
    scriptEl.addEventListener('error', reject);
    scriptEl.src = `${url}${params ? `?${new URLSearchParams(params).toString()}` : ''}`;
    document.body.appendChild(scriptEl);
  });
}

export function connectExtensionWythStaticUrl(url: string): TInitFn {
  // FIXME: fix rule
  // eslint-disable-next-line no-async-promise-executor
  return new Promise(async (resolve, reject) => {
    let rejected = false;
    const rejectFn: (rejectValue: any) => void = (rejectValue: any) => {
      if (rejected) {
        return;
      }
      rejected = true;
      reject(rejectValue);
    };
    try {
      setTimeout(() => rejectFn(new Error('load timeout')), 10000);
      await loadScript(url);
      // TODO: fallback url 'chrome-extension://epebfcehmdedogndhlcacafjaacknbcm/nmcades_plugin_api.js'
      //tslint:disable-line no-typeof-undefined
      // @ts-ignore
      const pluginObject = await cpcsp_chrome_nmcades.CreatePluginObject();
      const about = await pluginObject.CreateObjectAsync('CAdESCOM.About');
      console.log('Plugin CSPName ', await about.CSPName());
      resolve(pluginObject);
    } catch (error) {
      rejectFn(error || 'Плагин недоступен!');
    }
  });
}

export function connectWithDynamicUrl(): TInitFn {
  // FIXME: fix rule
  // eslint-disable-next-line no-async-promise-executor
  return new Promise(async (resolve, reject) => {
    let rejected = false;
    const rejectFn: (rejectValue: any) => void = (rejectValue: any) => {
      if (rejected) {
        return;
      }
      rejected = true;
      reject(rejectValue);
    };
    const handler: (evt: MessageEvent) => Promise<void> = async evt => {
      if (typeof evt.data !== 'string' || !evt.data.match('cadesplugin_loaded')) {
        return;
      }
      try {
        const resolvedUrl = evt.data.split('url:').slice(1).join('');
        console.log('Coming dynamic extension url ', resolvedUrl);
        await loadScript(resolvedUrl);
        // @ts-ignore FIXME
        const pluginObject = await cpcsp_chrome_nmcades.CreatePluginObject();
        const about = await pluginObject.CreateObjectAsync('CAdESCOM.About');
        console.log('Plugin CSPName ', await about.CSPName());
        resolve(pluginObject);
      } catch (error) {
        rejectFn(error);
      } finally {
        window.removeEventListener('message', handler, false);
      }
    };
    try {
      setTimeout(() => rejectFn(new Error('load timeout')), 10000);
      window.addEventListener('message', handler, false);
      window.postMessage('cadesplugin_echo_request', '*');
    } catch (err) {
      rejectFn(err);
    }
  });
}

declare global {
  interface Window {
    cadesplugin: any;
  }
  // @ts-ignore
  const cadesplugin: CadesPluginExtensionGlobalObject; //TODO: explain this и сделать как в старом eds exported
}

export class CadesPluginExtensionGlobalObject implements Promise<ICadesPluginAsync> {
  private _pluginPromise?: Promise<ICadesPluginAsync>;
  private readonly _initFn: TInitFn;

  readonly LOG_LEVEL_DEBUG: LogLevel = LogLevel.LOG_LEVEL_DEBUG;
  readonly LOG_LEVEL_INFO: LogLevel = LogLevel.LOG_LEVEL_INFO;
  readonly LOG_LEVEL_ERROR: LogLevel = LogLevel.LOG_LEVEL_ERROR;

  constructor(initFn: TInitFn) {
    this._initFn = initFn;
    // FIXME: убрать отсюда
    window.cadesplugin = this;
  }

  private get _plugin(): Promise<ICadesPluginAsync> {
    if (!this._pluginPromise) {
      this._pluginPromise = this._initFn;
    }
    return this._pluginPromise;
  }

  get [Symbol.toStringTag](): 'Promise' {
    return 'Promise';
  }

  then<TF = ICadesPluginAsync, TC = never>(
    onfulfilled?: ((value: ICadesPluginAsync) => TF | PromiseLike<TF>) | null | undefined,
    onrejected?: ((reason: any) => TC | PromiseLike<TC>) | null | undefined
  ): Promise<TF | TC> {
    return this._plugin.then(onfulfilled, onrejected);
  }

  catch<TC = never>(
    onrejected?: ((reason: any) => TC | PromiseLike<TC>) | null | undefined
  ): Promise<ICadesPluginAsync | TC> {
    return this._plugin.catch(onrejected);
  }

  finally(onfinally?: (() => void) | null | undefined): Promise<ICadesPluginAsync> {
    return this._plugin.finally(onfinally);
  }
}

// export function exportCadesObject<T extends object>(target: T, propertyName = 'cadesplugin'): T {
//   return Object.assign(target, { [propertyName]: {} });
// }
