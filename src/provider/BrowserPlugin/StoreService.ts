import type CadesBrowserPluginAsync from './CadesBrowserPluginAsync';
import type { Async, ICPCertificate } from './cadescom';
import { CAPICOM_CERTIFICATE_FIND_TYPE } from './capicom';

export interface IBrowserPluginStoreAsync {
  getCertificates(): Promise<TCertificatesMap>;
  getCertificate(identifier: TThumbprint): Promise<Async<ICPCertificate>>;
}

export type TThumbprint = string;

export type TCertificateItem = {
  serialNumber: string;
  thumbprint: TThumbprint;
  owner: string;
};

export type TCertificatesMap = Map<TThumbprint, TCertificateItem>;

export default class BrowserPluginStoreAsync implements IBrowserPluginStoreAsync {
  constructor(readonly _plugin: CadesBrowserPluginAsync) {}

  public async getCertificate(identifier: string): Promise<Async<ICPCertificate>> {
    const store = await this._plugin._providerInstance.CreateObjectAsync('CAdESCOM.Store');
    await store.Open();
    const certificatesObj = await store.Certificates;
    const findedCertObj = await certificatesObj.Find(
      CAPICOM_CERTIFICATE_FIND_TYPE.CAPICOM_CERTIFICATE_FIND_SHA1_HASH,
      identifier
    );
    const foundedNum = await findedCertObj.Count;
    if (!foundedNum) {
      throw new Error(`Certificate ${identifier} was not found in store`);
    }
    const certificateObj = await findedCertObj.Item(1);
    console.log('FOUNDED CERT ', identifier, await certificateObj.Thumbprint);
    if (!certificateObj) {
      throw new Error('Error while extract item at index 1');
    }
    return certificateObj;
  }

  public async getCertificates(): Promise<TCertificatesMap> {
    const store = await this._plugin._providerInstance.CreateObjectAsync('CAdESCOM.Store');
    await store.Open();
    const certificatesStoreObj = await store.Certificates;
    const totalNum = await certificatesStoreObj.Count;
    const certificateMap = new Map();
    if (!totalNum) {
      return certificateMap;
    }
    for (let i = 0; i < totalNum; i++) {
      const c = await certificatesStoreObj.Item(i + 1);
      const o: TCertificateItem = {
        serialNumber: await c.SerialNumber,
        thumbprint: await c.Thumbprint,
        owner: await c.SubjectName
      };
      certificateMap.set(o.thumbprint, o);
    }

    await store.Close();
    return certificateMap;
  }
}
