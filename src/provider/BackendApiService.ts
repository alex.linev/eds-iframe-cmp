import {
    IBackendApiService,
    IBackendApiServiceProvider,
    TSignConfiguration,
    TSignContext
} from '@/types';

export default class BackendApiService implements IBackendApiService {

    // TODO: inject
    constructor(
        private readonly signConfigurationProvider: IBackendApiServiceProvider
    ) {
    }

    getSignConfiguration(ctx: TSignContext): Promise<TSignConfiguration> {
        return this.signConfigurationProvider.provideSignConfiguration(ctx);
    }

}