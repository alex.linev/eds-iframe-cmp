import { createApp } from 'vue'
import App from '@/app/App.vue'
import { createCds, components, directives } from '@central-design-system/components';
import {icons} from '@central-design-system/icons';
import '@central-design-system/components/dist/cds.css';

const cds = createCds({
    components,
    directives,
    icons
    // ...options
});

const app = createApp(App);
app.use(cds)
app.mount('#app');