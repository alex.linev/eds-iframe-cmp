import type { IBackendApiServiceProvider, TSignContext } from '../types';
export default function useCreateSignedDoc(configProvider: IBackendApiServiceProvider): {
    signedDoc: import("vue").Ref<{
        doc_id: number;
    } | null>;
    createSignedDoc: (ctx: TSignContext) => Promise<void>;
    isSignedDocCreating: import("vue").Ref<boolean>;
    createSignedDocError: import("vue").Ref<{
        name: string;
        message: string;
        stack?: string | undefined;
        cause?: unknown;
    } | null>;
};
