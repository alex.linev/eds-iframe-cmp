export declare function useBrowserPluginConnector(): {
    env: import("vue").Ref<{
        readonly pluginConnectionInfo: {
            type: import('../provider/BrowserPlugin').EPluginConnectionType.ExtensionWithStaticUrl;
            connectionUrl: import('../provider/BrowserPlugin').EPluginConnectionStaticUrl;
        } | {
            type: import('../provider/BrowserPlugin').EPluginConnectionType.ExtensionWithDynamicUrl;
        } | {
            type: import('../provider/BrowserPlugin').EPluginConnectionType.NPAPI;
        };
        readonly canPromise: boolean;
        toJson: () => string;
    } | null>;
    getBrowserPluginFactory: () => Promise<void>;
    browserPluginFactory: import("vue").Ref<{
        readonly _providerInstance: {
            CreateObjectAsync: <T extends keyof import('../provider/BrowserPlugin').IObjectNamesMapAsync>(objName: T) => Promise<import('../provider/BrowserPlugin').IObjectNamesMapAsync[T]>;
            ReleasePluginObjects: () => Promise<boolean>;
        };
        readonly i: {
            CreateObjectAsync: <T extends keyof import('../provider/BrowserPlugin').IObjectNamesMapAsync>(objName: T) => Promise<import('../provider/BrowserPlugin').IObjectNamesMapAsync[T]>;
            ReleasePluginObjects: () => Promise<boolean>;
        };
    } | null>;
    connectBrowserPluginFactoryError: import("vue").Ref<{
        name: string;
        message: string;
        stack?: string | undefined;
        cause?: unknown;
    } | null>;
};
