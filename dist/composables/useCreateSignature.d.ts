import type { TSignContext } from '../types';
import type { ISignServiceAsync } from '../provider/BrowserPlugin/SignService';
export declare function useCreateSignature(signService: ISignServiceAsync): {
    isSigning: import("vue").Ref<boolean>;
    signature: import("vue").Ref<string | null>;
    createSignature: (ctx: TSignContext) => Promise<void>;
    createSignatureError: import("vue").Ref<{
        name: string;
        message: string;
        stack?: string | undefined;
        cause?: unknown;
    } | null>;
};
