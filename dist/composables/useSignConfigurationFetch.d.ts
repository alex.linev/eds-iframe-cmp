import type { IBackendApiServiceProvider, TSignContext } from '../types';
export default function useSignConfigurationFetch(configProvider: IBackendApiServiceProvider): {
    signConfiguration: import("vue").Ref<{
        context: {
            content: {
                type: "text";
                unsigned_message: string;
            } | {
                type: "files";
                fileIds: number[];
            };
            strategy: {
                type: import('../types').ESignStrategy.Qualified;
                software: import('../types').ESignSoftware.CPro;
                certificate_identifier: string;
            } | {
                type: import('../types').ESignStrategy.Qualified;
                software: import('../types').ESignSoftware.NCA;
            } | {
                type: import('../types').ESignStrategy.Simple;
                software: import('../types').ESignSoftware.Fake;
                owner: string;
                org_name: string;
            } | {
                type: import('../types').ESignStrategy.Mock;
                software: import('../types').ESignSoftware.Fake;
            };
            signature?: string | undefined;
            error?: {
                name: string;
                message: string;
                stack?: string | undefined;
                cause?: unknown;
            } | undefined;
            doc?: {
                doc_id: number;
            } | undefined;
        };
        qualified?: {
            identifier_type: string;
            valid_certificate_identifiers: string[];
        } | undefined;
    } | null>;
    fetchSignConfiguration: (ctx: TSignContext) => Promise<void>;
    isSignConfigurationFetching: import("vue").Ref<boolean>;
    signConfigurationFetchError: import("vue").Ref<{
        name: string;
        message: string;
        stack?: string | undefined;
        cause?: unknown;
    } | null>;
};
