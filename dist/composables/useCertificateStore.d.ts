import type { IBrowserPluginStoreAsync, TCertificatesMap } from '../provider/BrowserPlugin/StoreService';
export default function useCertificateListFetch(store: IBrowserPluginStoreAsync): {
    isCertificatesFetching: import("vue").Ref<boolean>;
    certificates: import("vue").Ref<(Map<string, {
        serialNumber: string;
        thumbprint: string;
        owner: string;
    }> & Omit<TCertificatesMap, keyof Map<any, any>>) | null>;
    fetchCertificateList: () => Promise<void>;
    certificatesFetchError: import("vue").Ref<{
        name: string;
        message: string;
        stack?: string | undefined;
        cause?: unknown;
    } | null>;
};
