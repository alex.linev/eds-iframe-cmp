type TWidgetConfig = {
    'backendApiUrl': string;
};
export type TWidgetInitConfig = {
    elementId: string;
    options: TWidgetConfig;
};
export declare function initWidget(cfg: TWidgetInitConfig): void;
export {};
