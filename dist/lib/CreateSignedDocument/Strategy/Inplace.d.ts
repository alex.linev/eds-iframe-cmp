import type { TSignConfiguration, TCreateSignedDocumentStrategy, TSignContext } from '../../../types';
/**
 * @author API Team <Api-dev-team@b2b-center.ru>
 */
export default class Inplace implements TCreateSignedDocumentStrategy {
    readonly config: any;
    readonly signHandler: (signature: string) => void;
    constructor(config: any, signHandler: (signature: string) => void);
    getSignConfiguration(): Promise<TSignConfiguration>;
    onSigned(context: TSignContext, signature: string): Promise<void>;
}
