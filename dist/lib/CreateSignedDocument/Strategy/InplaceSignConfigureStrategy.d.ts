import SignConfigureStrategy from './SignConfigureStrategy';
import type { TConfigureRequest, TCreateSignConfiguration, TSignSoftwareOptions } from '../../../types';
export default class InplaceSignConfigureStrategy extends SignConfigureStrategy {
    private readonly softCfg;
    constructor(softCfg: TSignSoftwareOptions);
    configure(cfg: TConfigureRequest): Promise<TCreateSignConfiguration>;
}
