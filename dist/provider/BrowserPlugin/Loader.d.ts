import CadesBrowserPluginAsync from './CadesBrowserPluginAsync';
declare enum LogLevel {
    LOG_LEVEL_DEBUG = 4,
    LOG_LEVEL_INFO = 2,
    LOG_LEVEL_ERROR = 1
}
export declare class CadesBrowserPluginLoader implements Promise<CadesBrowserPluginAsync> {
    private _pluginPromise?;
    readonly LOG_LEVEL_DEBUG: LogLevel;
    readonly LOG_LEVEL_INFO: LogLevel;
    readonly LOG_LEVEL_ERROR: LogLevel;
    current_log_level: number;
    private get _plugin();
    get [Symbol.toStringTag](): 'Promise';
    then<TF = CadesBrowserPluginAsync, TC = never>(onFulfilled?: ((value: CadesBrowserPluginAsync) => PromiseLike<TF> | TF) | null, onRejected?: ((reason: any) => PromiseLike<TC> | TC) | null): Promise<TF | TC>;
    catch<TC = never>(onRejected?: ((reason: any) => PromiseLike<TC> | TC) | null): Promise<CadesBrowserPluginAsync | TC>;
    finally(onFinally?: (() => void) | undefined | null): Promise<CadesBrowserPluginAsync>;
}
export {};
