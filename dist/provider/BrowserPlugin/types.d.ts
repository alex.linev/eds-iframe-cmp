export * from './capicom';
export * from './cadescom';
import type { IAbout, IAboutAsync, ICadesSignedData, ICadesSignedDataAsync, ICPAttribute, ICPAttributeAsync, ICPEnvelopedData, ICPEnvelopedDataAsync, ICPHashedData, ICPHashedDataAsync, ICPSigner, ICPSignerAsync, ICPStore, ICPStoreAsync, IRawSignature, IRawSignatureAsync, ISignedXML, ISignedXMLAsync } from './cadescom';
export interface IObjectNamesMapSync {
    'CAdESCOM.Store': ICPStore;
    'CAdESCOM.CPEnvelopedData': ICPEnvelopedData;
    'CAdESCOM.CPSigner': ICPSigner;
    'CAdESCOM.About': IAbout;
    'CAdESCOM.SignedXML': ISignedXML;
    'CAdESCOM.HashedData': ICPHashedData;
    'CAdESCOM.CadesSignedData': ICadesSignedData;
    'CAdESCOM.CPAttribute': ICPAttribute;
    'CAdESCOM.RawSignature': IRawSignature;
}
export interface IObjectNamesMapAsync {
    'CAdESCOM.Store': ICPStoreAsync;
    'CAdESCOM.CPEnvelopedData': ICPEnvelopedDataAsync;
    'CAdESCOM.CPSigner': ICPSignerAsync;
    'CAdESCOM.About': IAboutAsync;
    'CAdESCOM.SignedXML': ISignedXMLAsync;
    'CAdESCOM.HashedData': ICPHashedDataAsync;
    'CAdESCOM.CadesSignedData': ICadesSignedDataAsync;
    'CAdESCOM.CPAttribute': ICPAttributeAsync;
    'CAdESCOM.RawSignature': IRawSignatureAsync;
}
export declare const enum LogLevel {
    LOG_LEVEL_DEBUG = 4,
    LOG_LEVEL_INFO = 2,
    LOG_LEVEL_ERROR = 1
}
