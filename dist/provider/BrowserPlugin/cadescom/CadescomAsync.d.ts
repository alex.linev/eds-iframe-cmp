/**
 * @author API Team <api-dev-team@b2b-center.ru>
 */
import type { CAPICOM_CERTIFICATE_INCLUDE_OPTION, CAPICOM_ENCODING_TYPE, CAPICOM_HASH_ALGORITHM, ICertificateAsync } from '../index';
import type { IAbout, ICadesSignedData, ICPAttribute, ICPCertificate, ICPCertificates, ICPEnvelopedData, ICPHashedData, ICPRecipients, ICPSigner, ICPSigners, ICPStore, IPrivateKey, IRawSignature, ISignedXML, IVersion } from './CadescomSync';
import type { CADESCOM_ATTRIBUTE, CADESCOM_CONTENT_ENCODING_TYPE, CADESCOM_DISPLAY_DATA, CADESCOM_ENCRYPTION_ALGORITHM, CADESCOM_XML_SIGNATURE_TYPE } from './CadescomEnums';
import type { ICertificate, ICertificates, ICertificatesAsync, IEncodedData, IEncodedDataAsync, IOID, IOIDAsync, IPublicKey, IPublicKeyAsync, IStore, IStoreAsync } from '../capicom/';
export type Unpacked<T> = T extends Promise<infer U> ? U : T extends ICPStore<infer R> ? ICPStoreAsync<R> : T extends IStore<infer R> ? IStoreAsync<R> : T extends ICPCertificate ? ICPCertificateAsync : T extends ICertificate ? ICertificateAsync : T extends ICPCertificates<infer R> ? ICPCertificatesAsync<R> : T extends ICertificates<infer R> ? ICertificatesAsync<R> : T extends ICPSigner ? ICPSignerAsync : T extends ICPHashedData ? ICPHashedDataAsync : T extends ICPRecipients ? ICPRecipientsAsync : T extends IVersion ? IVersionAsync : T extends IOID ? IOIDAsync : T extends IEncodedData ? IEncodedDataAsync : T extends IPublicKey ? IPublicKeyAsync : T extends IPrivateKey ? IPrivateKeyAsync : T;
export type PromisifiedFunction<T extends Function> = T extends (...args: infer A) => infer U ? (...args: {
    [K in keyof A]: Unpacked<A[K]>;
}) => Promise<Unpacked<U>> : T;
export type Async<T> = {
    readonly [K in keyof T]: T[K] extends Function ? PromisifiedFunction<T[K]> : Promise<Unpacked<T[K]>>;
};
export interface IPrivateKeyAsync extends Async<IPrivateKey> {
    propset_CachePin(isCached: boolean): Promise<void>;
}
export type ICPCertificateAsync = Async<ICPCertificate>;
export type ICPRecipientsAsync = Async<ICPRecipients>;
export interface ICPEnvelopedDataAsync extends Async<ICPEnvelopedData> {
    propset_Algorithm(algorithm: CADESCOM_ENCRYPTION_ALGORITHM): Promise<void>;
    propset_Content(content: string): Promise<void>;
}
export interface ICPSignerAsync extends Async<ICPSigner> {
    propset_Certificate(certificate: ICertificateAsync): Promise<void>;
    propset_CheckCertificate(checkCertificate: boolean): Promise<void>;
    propset_KeyPin(keyPin: string): Promise<void>;
    propset_Options(options: CAPICOM_CERTIFICATE_INCLUDE_OPTION): Promise<void>;
    propset_TSAAddress(TSAAddress: string): Promise<void>;
}
export interface ICadesSignedDataAsync extends Async<ICadesSignedData> {
    propset_DisplayData(displayData: CADESCOM_DISPLAY_DATA): Promise<void>;
    propset_Content(content: string): Promise<void>;
    propset_ContentEncoding(contentEncoding: CADESCOM_CONTENT_ENCODING_TYPE): Promise<void>;
}
export type IVersionAsync = Async<IVersion>;
export type IAboutAsync = Async<IAbout>;
export type ICPSignersAsync = Async<ICPSigners>;
export interface ISignedXMLAsync extends Async<ISignedXML> {
    propset_Content(content: string): Promise<void>;
    propset_DigestMethod(digestMethod: string): Promise<void>;
    propset_SignatureMethod(signatureMethod: string): Promise<void>;
    propset_SignatureType(signatureType: CADESCOM_XML_SIGNATURE_TYPE): Promise<void>;
}
export interface ICPHashedDataAsync extends Async<ICPHashedData> {
    propset_Algorithm(algorithm: CAPICOM_HASH_ALGORITHM): Promise<void>;
    propset_DataEncoding(dataEncoding: CADESCOM_CONTENT_ENCODING_TYPE): Promise<void>;
}
export interface ICPAttributeAsync extends Async<ICPAttribute> {
    propset_Name(name: CADESCOM_ATTRIBUTE): Promise<void>;
    propset_Value(value: any): Promise<void>;
    propset_ValueEncoding(valueEncoding: CAPICOM_ENCODING_TYPE): Promise<void>;
}
export type IRawSignatureAsync = Async<IRawSignature>;
export type ICPCertificatesAsync<T = ICPCertificate> = Async<ICPCertificates<T>>;
export type ICPStoreAsync<T = ICPCertificates> = Async<ICPStore<T>>;
