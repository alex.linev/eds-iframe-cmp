import { IEnvironment } from './Environment';
import { type ICadesPluginAsync } from './CadesExtensionExposeObject';
/**
 * @author API Team <api-dev-team@b2b-center.ru>
 */
export default class CadesBrowserPluginAsync {
    readonly _providerInstance: ICadesPluginAsync;
    private constructor();
    static initByEnvironment(environment: IEnvironment): Promise<CadesBrowserPluginAsync>;
    static createWithStaticUrl(url: string): Promise<CadesBrowserPluginAsync>;
    static createWithUrlResolve(): Promise<CadesBrowserPluginAsync>;
    get i(): ICadesPluginAsync;
}
