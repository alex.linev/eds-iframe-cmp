/**
 * @author API Team <api-dev-team@b2b-center.ru>
 */
import { type TSignature } from './types';
export declare enum EOperationStatusValue {
    IDLE = 0,
    PENDING = 1,
    DONE = 2
}
export declare enum EOperationResultType {
    Failed = 0,
    Ok = 1
}
export type TOperationResultError = Error;
export type TOperationResult<R> = {
    type: EOperationResultType.Failed;
    error: TOperationResultError;
} | {
    type: EOperationResultType.Ok;
    value: R;
};
export type TOperationState<R> = {
    status: EOperationStatusValue.IDLE;
} | {
    status: EOperationStatusValue.PENDING;
    description: string;
} | {
    status: EOperationStatusValue.DONE;
    result: TOperationResult<R>;
};
export declare function newOperationState<R>(): TOperationState<R>;
export declare function newSignOperationState(ctx: TSignContext): TSignOperationState;
export type TSignOperationResult = {
    signature: TSignature;
};
export type TSignOperationState = TOperationState<TSignOperationResult> & {
    context: TSignContext;
};
export type TSignWithCertificateSelectionOperationState = TSignOperationState & {
    certificates: [];
    selectedCertificateIdentifier: string;
};
export type TSignSimpleState = TSignOperationState & {
    pin: string;
};
export type TLoadCertificatesResult<CT> = {
    certificates: CT[];
};
export declare enum ESignContentType {
    TEXT = 0,
    HASH = 1
}
export declare enum EContentEncodingType {
    NONE = 0,
    BASE64 = 1
}
export declare enum ESignSoftware {
    CPRO = 0,
    NCA = 1,
    SIMPLE = 2
}
export type TSignTextContext = {
    type: ESignContentType.TEXT;
    text: string;
    encoding: EContentEncodingType;
    software: ESignSoftware;
};
export type TSignHashContext = {
    type: ESignContentType.HASH;
    hash: string;
    software: ESignSoftware;
};
export type TSignContext = TSignTextContext | TSignHashContext;
