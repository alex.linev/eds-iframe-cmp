import { type TSignContext, type TSignOperationState } from './SignState';
export default class SignModel {
    private _state;
    private constructor();
    static new(ctx: TSignContext): SignModel;
    get state(): TSignOperationState;
    signText(text: string, certificateIdentifier: string): Promise<void>;
}
