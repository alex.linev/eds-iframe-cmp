/**
 * @author API Team <api-dev-team@b2b-center.ru>
 */
import type SignService from '../../SignService';
import type { TCertificatesMap, TThumbprint } from '../../StoreService';
import type BrowserPluginStoreAsync from '../../StoreService';
import type { TSignature } from '../model/types';
export interface ISignWithCertificateView {
    onInited(): void;
    onError(error: Error): void;
    onCertificatesLoaded(certificates: TCertificatesMap): void;
    onSigned(signature: TSignature): void;
}
export interface ISignWithCertificatePresenter {
    onInit(): void;
    onSign(text: string, certificate: TThumbprint): Promise<void>;
}
export default class SignWithCertificateSelectPresenter implements ISignWithCertificatePresenter {
    private readonly _signService;
    private readonly _storeService;
    private readonly _view;
    constructor(view: ISignWithCertificateView, signSrv: SignService, storeSrv: BrowserPluginStoreAsync);
    onSign(text: string, certificate: string): Promise<void>;
    onInit(): Promise<void>;
    onCertificatesLoad(): Promise<void>;
}
