import type CadesBrowserPluginAsync from './CadesBrowserPluginAsync';
import type { Async, ICPCertificate } from './cadescom';
export interface IBrowserPluginStoreAsync {
    getCertificates(): Promise<TCertificatesMap>;
    getCertificate(identifier: TThumbprint): Promise<Async<ICPCertificate>>;
}
export type TThumbprint = string;
export type TCertificateItem = {
    serialNumber: string;
    thumbprint: TThumbprint;
    owner: string;
};
export type TCertificatesMap = Map<TThumbprint, TCertificateItem>;
export default class BrowserPluginStoreAsync implements IBrowserPluginStoreAsync {
    readonly _plugin: CadesBrowserPluginAsync;
    constructor(_plugin: CadesBrowserPluginAsync);
    getCertificate(identifier: string): Promise<Async<ICPCertificate>>;
    getCertificates(): Promise<TCertificatesMap>;
}
