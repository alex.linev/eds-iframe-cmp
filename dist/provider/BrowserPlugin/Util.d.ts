/**
 * @author API Team <api-dev-team@b2b-center.ru>
 */
export interface ILoadedScript {
    remove(): void;
}
export declare function loadScript(url: string, params?: string | [string, string][] | URLSearchParams | null, scriptElementOptions?: Partial<HTMLScriptElement>): Promise<ILoadedScript>;
