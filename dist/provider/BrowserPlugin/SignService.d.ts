import type CadesBrowserPluginAsync from './CadesBrowserPluginAsync';
import type { IBrowserPluginStoreAsync, TThumbprint } from './StoreService';
import type { TSignature } from '../../types';
export interface ISignServiceAsync {
    signText(text: string, certificateIdentifier: TThumbprint): Promise<TSignature>;
}
export default class SignService implements ISignServiceAsync {
    private readonly _plugin;
    private readonly _store;
    constructor(_plugin: CadesBrowserPluginAsync, _store: IBrowserPluginStoreAsync);
    signText(text: string, certificateIdentifier: TThumbprint): Promise<TSignature>;
}
