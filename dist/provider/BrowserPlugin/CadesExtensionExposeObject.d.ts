import { type IObjectNamesMapAsync, LogLevel } from './types';
export interface ICadesPluginAsync {
    CreateObjectAsync<T extends keyof IObjectNamesMapAsync>(objName: T): Promise<IObjectNamesMapAsync[T]>;
    ReleasePluginObjects(): Promise<boolean>;
}
type TInitFn = Promise<ICadesPluginAsync>;
export declare function connectExtensionWythStaticUrl(url: string): TInitFn;
export declare function connectWithDynamicUrl(): TInitFn;
declare global {
    interface Window {
        cadesplugin: any;
    }
    const cadesplugin: CadesPluginExtensionGlobalObject;
}
export declare class CadesPluginExtensionGlobalObject implements Promise<ICadesPluginAsync> {
    private _pluginPromise?;
    private readonly _initFn;
    readonly LOG_LEVEL_DEBUG: LogLevel;
    readonly LOG_LEVEL_INFO: LogLevel;
    readonly LOG_LEVEL_ERROR: LogLevel;
    constructor(initFn: TInitFn);
    private get _plugin();
    get [Symbol.toStringTag](): 'Promise';
    then<TF = ICadesPluginAsync, TC = never>(onfulfilled?: ((value: ICadesPluginAsync) => TF | PromiseLike<TF>) | null | undefined, onrejected?: ((reason: any) => TC | PromiseLike<TC>) | null | undefined): Promise<TF | TC>;
    catch<TC = never>(onrejected?: ((reason: any) => TC | PromiseLike<TC>) | null | undefined): Promise<ICadesPluginAsync | TC>;
    finally(onfinally?: (() => void) | null | undefined): Promise<ICadesPluginAsync>;
}
export {};
