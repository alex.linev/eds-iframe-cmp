type UNKNOWN_BROWSER_PARAM = 'unknown';
type TBrowserInfo = {
    name: string | UNKNOWN_BROWSER_PARAM;
    version: string | UNKNOWN_BROWSER_PARAM;
};
type TOsInfo = {
    name: string | UNKNOWN_BROWSER_PARAM;
    version?: string | UNKNOWN_BROWSER_PARAM;
    bits?: string;
};
export declare enum EPluginConnectionType {
    ExtensionWithStaticUrl = "extension_with_static_url",
    ExtensionWithDynamicUrl = "extension_with_dynamic_url",
    NPAPI = "npapi"
}
export declare enum EPluginConnectionStaticUrl {
    Chrome = "chrome-extension://iifchhfnnmpdbibifmljnfjhpififfog/nmcades_plugin_api.js",
    Opera = "chrome-extension://epebfcehmdedogndhlcacafjaacknbcm/nmcades_plugin_api.js"
}
export type TPluginConnectionInfo = {
    type: EPluginConnectionType.ExtensionWithStaticUrl;
    connectionUrl: EPluginConnectionStaticUrl;
} | {
    type: EPluginConnectionType.ExtensionWithDynamicUrl;
} | {
    type: EPluginConnectionType.NPAPI;
};
export type TEnvironment = {
    pluginConnectionType: TPluginConnectionInfo;
    canPromise: boolean;
    systemInfo: {
        browser: TBrowserInfo;
        os: TOsInfo;
    };
};
export interface IEnvironment {
    get pluginConnectionInfo(): TPluginConnectionInfo;
    get canPromise(): boolean;
    toJson(): string;
}
export declare class Environment implements IEnvironment {
    private readonly env;
    private static _instance;
    private constructor();
    static i(): IEnvironment;
    static createByBrowser(): Environment;
    get pluginConnectionInfo(): TPluginConnectionInfo;
    get canPromise(): boolean;
    toJson(): string;
}
export {};
