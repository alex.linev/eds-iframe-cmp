import { IBackendApiServiceProvider, TSignConfiguration, TSignContext } from '../types';
export default class BackendRestJsApiProvider implements IBackendApiServiceProvider {
    private readonly restOpenApiModel;
    constructor(accessToken?: string);
    provideSignConfiguration(ctx: TSignContext): Promise<TSignConfiguration>;
    createSignedDocument(context: TSignContext): Promise<void>;
}
