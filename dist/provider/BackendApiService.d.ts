import { IBackendApiService, IBackendApiServiceProvider, TSignConfiguration, TSignContext } from '../types';
export default class BackendApiService implements IBackendApiService {
    private readonly signConfigurationProvider;
    constructor(signConfigurationProvider: IBackendApiServiceProvider);
    getSignConfiguration(ctx: TSignContext): Promise<TSignConfiguration>;
}
