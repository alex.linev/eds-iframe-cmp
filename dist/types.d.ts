export type TSignedDocument = {
    doc_id: number;
};
export type TFileId = number;
export type TSignContentType = 'text' | 'files';
export type TSignTextContext = {
    type: 'text';
    text: string;
};
export type TSignFilesContext = {
    type: 'files';
    file_ids: TFileId[];
};
export type TSignContext_OLD = (TSignTextContext | TSignFilesContext) & {
    result?: TSignedDocument;
    error?: Error;
};
export type TSignConfiguration = {
    context: TSignContext;
    qualified?: TSignConfigQualified;
};
export type TSignConfigQualified = {
    identifier_type: string;
    valid_certificate_identifiers: string[];
};
export type TCreateSignedDocumentStrategy = {
    getSignConfiguration(context: TSignContext): Promise<TSignConfiguration>;
    onSigned(context: TSignContext, signature: string): Promise<void>;
};
export type TCreateSignedDocumentStatusValue = 'created' | 'initialized' | 'waited' | 'processing' | 'done' | 'error';
export type TCreateSignedDocumentStatus = {
    value: TCreateSignedDocumentStatusValue;
    message: string;
};
export type TSignature = string;
export type TThumbprint = string;
export type FileId = number;
export type DocId = number;
export type TError = Error;
export type TCertificate = {
    serialNumber: string;
    thumbprint: TThumbprint;
    owner: string;
    issuer: string;
    validFrom: string;
    validTo: string;
};
export type TCertificateIdentifier = TThumbprint;
export declare enum ESignSoftware {
    CPro = "cpro",
    NCA = "nca",
    Fake = "fake"
}
export declare enum ESignStrategy {
    Qualified = "qualified",
    Simple = "simple",
    Mock = "mock"
}
export type TSignTextContent = {
    type: 'text';
    unsigned_message: string;
};
export type TSignFilesContent = {
    type: 'files';
    fileIds: FileId[];
};
export type TSignContent = TSignTextContent | TSignFilesContent;
export type TSignCProStrategy = {
    type: ESignStrategy.Qualified;
    software: ESignSoftware.CPro;
    certificate_identifier: TCertificateIdentifier;
};
export type TSignNcaStrategy = {
    type: ESignStrategy.Qualified;
    software: ESignSoftware.NCA;
};
export type TSignSimpleStrategy = {
    type: ESignStrategy.Simple;
    software: ESignSoftware.Fake;
    owner: string;
    org_name: string;
};
export type TSignMockStrategy = {
    type: ESignStrategy.Mock;
    software: ESignSoftware.Fake;
};
export type TSignStrategy = TSignCProStrategy | TSignNcaStrategy | TSignSimpleStrategy | TSignMockStrategy;
export type TSignContext = {
    content: TSignContent;
    strategy: TSignStrategy;
    signature?: TSignature;
    error?: TError;
    doc?: TSignedDocument;
};
export interface ISignServiceService {
}
export type TSignQualifiedConfig = {
    allowed_software: ESignSoftware[];
    valid_certificates: TCertificateIdentifier[];
};
export type TSignSimpleConfig = {
    owner: string;
    org_name: string;
};
export type TSignMockConfig = {};
export type TSignConfig = {
    qualified?: TSignQualifiedConfig;
    simple?: TSignSimpleConfig;
    mock?: TSignMockConfig;
};
export interface ISignedDocumentService {
}
export interface ISignatureService {
}
export interface IBackendApiService {
    getSignConfiguration(ctx: TSignContext): Promise<TSignConfiguration>;
}
export interface IBackendApiServiceProvider {
    provideSignConfiguration(ctx: TSignContext): Promise<TSignConfiguration>;
    createSignedDocument(context: TSignContext): Promise<void>;
}
export type TConfigureSignedTextDocumentCreateRequest = {};
export type TConfigureSignedFilesDocumentCreateRequest = {
    fileIds: FileId[];
};
export type TConfigureCosignDocumentCreateRequest = {
    prevDocId: TDocumentId;
};
export type TConfigureRequest = TConfigureSignedTextDocumentCreateRequest | TConfigureSignedFilesDocumentCreateRequest | TConfigureCosignDocumentCreateRequest;
export type TDocumentId = number;
export type TDocumentType = 'standard' | 'fs_hash' | 'simple' | 'mock';
export type THash = string;
export type TPreviousSignature = {
    docType: TDocumentType;
    software?: ESignSoftware;
    value: TSignature;
};
export type TSignSoftwareOptions = {
    qualified?: TSignQualifiedConfig;
    simple?: TSignSimpleConfig;
    mock?: TSignMockConfig;
};
export type TCreateTextSignatureConfiguration = {
    type: 'text';
    softwareOptions: TSignSoftwareOptions;
};
export type TCreateFilesSignatureTextConfiguration = {
    type: 'files';
    softwareOptions: TSignSoftwareOptions;
    fileHashes: Record<TFileId, THash>[];
};
export type TCreateCosignConfiguration = {
    type: 'cosign';
    softwareOptions: TSignSoftwareOptions;
    previousSignature: TPreviousSignature;
};
export type TCreateSignConfiguration = TCreateTextSignatureConfiguration | TCreateFilesSignatureTextConfiguration | TCreateCosignConfiguration;
export interface ISignConfigureStrategy {
    configure(cfg: TConfigureRequest): Promise<TCreateSignConfiguration>;
}
