export { default as CreateSignedDocumentModal } from './components/CreateSignedDocumentModal.vue';
export { initWidget, type TWidgetInitConfig } from './widgetInitializer';
